#pragma once
#include "SFML\Graphics.hpp"

class Boid {
public:
	sf::Vector2f pos;
	sf::Vector2f velocity;

	sf::CircleShape shape;


	Boid(const sf::Vector2f& pos, const sf::Color& color = sf::Color::Red);
	void Update(float dt);
	void Draw(sf::RenderWindow& window);

	void SetVelocity(const sf::Vector2f& vel);

	const sf::Vector2f& GetPos();
	const sf::Vector2f& GetVelocity();
};
