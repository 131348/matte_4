#include "Boids.h"
#include <random>
#include <iostream>

Boids::Boids() {
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<> distX(0, 640);
	std::uniform_int_distribution<> distY(0, 480);

	for (int i = 0; i < this->nrOfBoids; i++) {
		this->boids.emplace_back(Boid({ (float)distX(mt), (float)distY(mt) }));
		this->listOfboids.emplace_back(new Boid({ (float)distX(mt), (float)distY(mt) }));
	}
}

Boids::~Boids() {
	for (auto it : this->listOfboids) {
		delete it;
	}
}

void Boids::Update(float dt) {
	sf::Vector2f sumVelocities = this->nextStepSumVelocities;
	sf::Vector2f sumPositions = this->nextStepSumPositions;
	this->nextStepSumVelocities = { 0,0 };
	this->nextStepSumPositions = { 0,0 };
	std::vector<Boid*> copiedList = this->listOfboids;

	for (auto b : this->listOfboids) {
		sf::Vector2f centerOfMass = (sumPositions - b->pos) / (float)(this->nrOfBoids - 1);
		b->velocity += (centerOfMass - b->pos) / 100.f;


		sf::Vector2f averageVelocity = (sumVelocities - b->velocity) / (float)(nrOfBoids - 1);
		b->velocity += (averageVelocity - b->velocity) / 10.f;

		sf::Vector2f min{ 0.f, 0.f };
		sf::Vector2f max{ 640.f, 480.f };

		if (b->pos.x < min.x) {
			b->velocity.x += 10;
		}
		else if (b->pos.x > max.x) {
			b->velocity.x -= 10;
		}

		if (b->pos.y < min.y) {
			b->velocity.y += 10;
		}
		else if (b->pos.y > max.y) {
			b->velocity.y -= 10;
		}

		int lim = 50;

		if (std::sqrt(std::pow(b->velocity.x, 2) + std::pow(b->velocity.y, 2)) > lim) {
			b->velocity = (b->velocity / (std::sqrt(std::pow(b->velocity.x, 2) + std::pow(b->velocity.y, 2))));
		}

		for (auto c : copiedList) {
			sf::Vector2f vec(c->pos - b->pos);
			float dist = std::sqrt(std::pow(vec.x, 2) + std::pow(vec.y, 2));
			if (b != c && dist < 13.f) {
				b->velocity -= (c->pos - b->pos) / dist * 2.f;
				c->velocity += (c->pos - b->pos + b->velocity * dt / 200.f) / dist * 2.f;
			}
		}

		copiedList.erase(copiedList.begin());

		this->nextStepSumVelocities += b->velocity;
		this->nextStepSumPositions += b->pos + b->velocity * dt / 200.f;

		b->Update(dt);
	}
}


void Boids::Draw(sf::RenderWindow& window) {
	for (auto& it : this->listOfboids) {
		it->Draw(window);
	}
}



