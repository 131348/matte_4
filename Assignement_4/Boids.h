#pragma once
#include <vector>
#include "SFML\Graphics.hpp"
#include "Boid.h"

class Boids {
private:
	const int nrOfBoids = 40;
	std::vector<Boid> boids;

	std::vector<Boid*> listOfboids;
	sf::Vector2f nextStepSumVelocities;
	sf::Vector2f nextStepSumPositions;


public:
	Boids();
	~Boids();

	void Update(float dt);
	void Draw(sf::RenderWindow& window);
};