#pragma once
#include "SFML\Graphics.hpp"
#include <iostream>
#include <cmath>

sf::VertexArray MidpointDisplacement(const sf::Vector2f& start, const sf::Vector2f& end, float roughness, float verticalDisplacement = 10, int iterations = 6) {
	if (!verticalDisplacement) {
		verticalDisplacement = (start.y + end.y) / 2;
	}
	int lastId = pow(2, iterations);
	sf::VertexArray vertexArray(sf::LinesStrip, lastId + 1);
	vertexArray[0].position = start;
	vertexArray[lastId].position = end;

	std::random_device rd;
	std::mt19937 mt(rd());

	for (int i = 1; i <= iterations; i++) {
		int idInc = lastId / pow(2, i - 1);
		std::uniform_int_distribution<> distY(-verticalDisplacement, verticalDisplacement);

		for (int id = 0; id < lastId; id += idInc) {
			sf::Vector2f midPoint = (vertexArray[id + idInc].position + vertexArray[id].position) / 2.f;
			midPoint.y += distY(mt);
			vertexArray[(id * 2 + idInc) / 2].position = midPoint;
		}
		verticalDisplacement *= pow(2, -roughness);
	}

	return vertexArray;
}


