#pragma once
#include <vector>

class PerlinNoise {
private:
	std::vector<int> p;

	double Fade(double t);
	double Lerp(double t, double a, double b);
	double Grad(int hash, double x, double y, double z);

public:
	PerlinNoise();

	double Noise(double x, double y, double z);
};

