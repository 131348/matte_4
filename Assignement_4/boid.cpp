#include "Boid.h"

Boid::Boid(const sf::Vector2f& pos, const sf::Color& color) : pos(pos) {
	this->velocity = { 10.f, 0.f };
	this->shape.setFillColor(color);
	this->shape.setRadius(5);
	this->shape.setOrigin(5, 5);
	this->shape.setPosition(this->pos);
}

void Boid::Update(float dt) {
	this->pos += this->velocity * (dt / 200);
	/*if (pos.x < 0 || pos.x > 640) {
	this->velocity.x *= -1;
	}
	if (pos.y < 0 || pos.y > 480) {
	this->velocity.y *= -1;
	}*/

	this->shape.setPosition(this->pos);
}

void Boid::Draw(sf::RenderWindow& window) {
	window.draw(this->shape);
}

void Boid::SetVelocity(const sf::Vector2f& vel) {
	this->velocity = vel;
}

const sf::Vector2f& Boid::GetPos() {
	return this->pos;
}

const sf::Vector2f& Boid::GetVelocity() {
	return this->velocity;
}